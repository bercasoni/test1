#FROM node:lts-slim
FROM node:14-alpine
#FROM node:10-alpine

WORKDIR /usr/src/app
#WORKDIR /app

COPY /app/package*.json ./
RUN npm install
COPY /app .
EXPOSE 4000

#CMD [ "npm", "start" ]
CMD ["index.js"]
