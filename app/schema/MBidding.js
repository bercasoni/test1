cube(`MBidding`, {
  sql: `SELECT * FROM public.m_bidding`,
  
  preAggregations: {
    // Pre-Aggregations definitions go here
    // Learn more here: https://cube.dev/docs/caching/pre-aggregations/getting-started  
  },
  
  joins: {
    MPrequalificationInformation: {
      sql: `${CUBE}.prequalification_id = ${MPrequalificationInformation}.id`,
      relationship: `hasOne`
    },
    MBiddingSubmission: {
      sql: `${CUBE}.id = ${MBiddingSubmission}.bidding_id`,
      relationship: `hasMany`
    },
    MVendorConfirmation: {
      sql: `${CUBE}.id = ${MVendorConfirmation}.bidding_id`,
      relationship: `hasOne`
    },
    MPrequalificationNegotiation: {
      sql: `${CUBE}.prequalification_id = ${MPrequalificationNegotiation}.id`,
      relationship: `hasOne`
    },
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [uid, id, name, createdBy, biddingStatus, createdDate, dateReject, dateApprove, dateTrx, lastModifiedDate]
    },
    
    estimatedPrice: {
      sql: `estimated_price`,
      type: `sum`
    },
    
    ceilingPrice: {
      sql: `ceiling_price`,
      type: `sum`
    },
    
    joinedVendorCount: {
      sql: `joined_vendor_count`,
      type: `sum`
    }
  },
  
  dimensions: {
    approved: {
      sql: `approved`,
      type: `string`
    },
    
    processed: {
      sql: `processed`,
      type: `string`
    },
    
    active: {
      sql: `active`,
      type: `string`
    },
    
    uid: {
      sql: `uid`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `number`,
      primaryKey: true
    },
    
    name: {
      sql: `name`,
      type: `string`
    },
    
    vendorSelection: {
      sql: `vendor_selection`,
      type: `string`
    },
    
    documentAction: {
      sql: `document_action`,
      type: `string`
    },
    
    documentStatus: {
      sql: `document_status`,
      type: `string`
    },
    
    createdBy: {
      sql: `created_by`,
      type: `string`
    },
    
    lastModifiedBy: {
      sql: `last_modified_by`,
      type: `string`
    },
    
    documentNo: {
      sql: `document_no`,
      type: `string`
    },
    
    rejectedReason: {
      sql: `rejected_reason`,
      type: `string`
    },
    
    biddingStatus: {
      sql: `bidding_status`,
      type: `string`
    },
    
    createdDate: {
      sql: `created_date`,
      type: `time`
    },
    
    dateReject: {
      sql: `date_reject`,
      type: `time`
    },
    
    dateApprove: {
      sql: `date_approve`,
      type: `time`
    },
    
    dateTrx: {
      sql: `date_trx`,
      type: `time`
    },
    
    lastModifiedDate: {
      sql: `last_modified_date`,
      type: `time`
    }
  },
  
  dataSource: `default`
});
