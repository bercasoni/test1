cube(`CCostCenter`, {
  sql: `SELECT * FROM public.c_cost_center`,
  
  preAggregations: {
    // Pre-Aggregations definitions go here
    // Learn more here: https://cube.dev/docs/caching/pre-aggregations/getting-started  
  },
  
  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [name, uid, createdBy, id, createdDate, lastModifiedDate]
    }
  },
  
  dimensions: {
    description: {
      sql: `description`,
      type: `string`
    },
    
    active: {
      sql: `active`,
      type: `string`
    },
    
    name: {
      sql: `name`,
      type: `string`
    },
    
    division: {
      sql: `division`,
      type: `string`
    },
    
    uid: {
      sql: `uid`,
      type: `string`
    },
    
    lastModifiedBy: {
      sql: `last_modified_by`,
      type: `string`
    },
    
    createdBy: {
      sql: `created_by`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `number`,
      primaryKey: true
    },
    
    code: {
      sql: `code`,
      type: `string`
    },
    
    createdDate: {
      sql: `created_date`,
      type: `time`
    },
    
    lastModifiedDate: {
      sql: `last_modified_date`,
      type: `time`
    }
  },
  
  dataSource: `default`
});
