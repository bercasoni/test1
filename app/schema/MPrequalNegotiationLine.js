cube(`MPrequalNegotiationLine`, {
  sql: `SELECT * FROM public.m_prequal_negotiation_line`,
  
  preAggregations: {
    // Pre-Aggregations definitions go here
    // Learn more here: https://cube.dev/docs/caching/pre-aggregations/getting-started  
  },
  
  joins: {
    MPrequalificationSubmission: {
      sql: `${CUBE}.submission_id = ${MPrequalificationSubmission}.id`,
      relationship: `hasOne`
    }
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [id, uid, createdBy, createdDate, lastModifiedDate]
    }
  },
  
  dimensions: {
    id: {
      sql: `id`,
      type: `number`,
      primaryKey: true
    },
    
    uid: {
      sql: `uid`,
      type: `string`
    },
    
    active: {
      sql: `active`,
      type: `string`
    },
    
    negotiationStatus: {
      sql: `negotiation_status`,
      type: `string`
    },
    
    createdBy: {
      sql: `created_by`,
      type: `string`
    },
    
    lastModifiedBy: {
      sql: `last_modified_by`,
      type: `string`
    },
    
    createdDate: {
      sql: `created_date`,
      type: `time`
    },
    
    lastModifiedDate: {
      sql: `last_modified_date`,
      type: `time`
    }
  },
  
  dataSource: `default`
});
