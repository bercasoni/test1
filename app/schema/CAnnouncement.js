cube(`CAnnouncement`, {
  sql: `SELECT * FROM public.c_announcement`,
  
  preAggregations: {
    // Pre-Aggregations definitions go here
    // Learn more here: https://cube.dev/docs/caching/pre-aggregations/getting-started  
  },
  
  joins: {
    MBiddingSchedule: {
      sql: `${CUBE}.bidding_schedule_id = ${MBiddingSchedule}.id`,
      relationship: `hasOne`
    },
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [id, uid, createdBy, createdDate, publishDate, lastModifiedDate]
    }
  },
  
  dimensions: {
    id: {
      sql: `id`,
      type: `number`,
      primaryKey: true
    },
    
    active: {
      sql: `active`,
      type: `string`
    },
    
    uid: {
      sql: `uid`,
      type: `string`
    },
    
    description: {
      sql: `description`,
      type: `string`
    },
    
    createdBy: {
      sql: `created_by`,
      type: `string`
    },
    
    lastModifiedBy: {
      sql: `last_modified_by`,
      type: `string`
    },
    
    createdDate: {
      sql: `created_date`,
      type: `time`
    },
    
    publishDate: {
      sql: `publish_date`,
      type: `time`
    },
    
    lastModifiedDate: {
      sql: `last_modified_date`,
      type: `time`
    }
  },
  
  dataSource: `default`
});
