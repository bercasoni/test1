cube(`MBiddingEvalResult`, {
  sql: `SELECT * FROM public.m_bidding_eval_result`,
  
  preAggregations: {
    // Pre-Aggregations definitions go here
    // Learn more here: https://cube.dev/docs/caching/pre-aggregations/getting-started  
  },
  
  joins: {
    MBiddingSubmission: {
      sql: `${CUBE}.bidding_submission_id = ${MBiddingSubmission}.id`,
      relationship: `belongsTo`
    }
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [id, uid, createdBy, createdDate, lastModifiedDate]
    }
  },
  
  dimensions: {
    id: {
      sql: `id`,
      type: `number`,
      primaryKey: true
    },
    
    uid: {
      sql: `uid`,
      type: `string`
    },
    
    active: {
      sql: `active`,
      type: `string`
    },
    
    winnerStatus: {
      sql: `winner_status`,
      type: `string`
    },
    
    status: {
      sql: `status`,
      type: `string`
    },
    
    createdBy: {
      sql: `created_by`,
      type: `string`
    },
    
    lastModifiedBy: {
      sql: `last_modified_by`,
      type: `string`
    },
    
    evaluationStatus: {
      sql: `evaluation_status`,
      type: `string`
    },

    finalEvaluationStatus: {
      case: {
        when: [
          {
            sql: `(select id from m_bidding_negotiation_line where bidding_eval_result_id = ${CUBE}.id) is not null`, 
            label: {
              sql: `case when (select id from m_vendor_confirmation_line where bidding_eval_result_id = ${CUBE}.id) is not null then 'Pass' else 'Fail' end`
            }
          },
        ],
        else: { label: `Fail` },
      },
      type: `string`,
      title: `Final Status`
    },
    
    createdDate: {
      sql: `created_date`,
      type: `time`
    },
    
    lastModifiedDate: {
      sql: `last_modified_date`,
      type: `time`
    }
  },
  
  dataSource: `default`
});
