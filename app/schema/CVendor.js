cube(`CVendor`, {
  sql: `SELECT * FROM public.c_vendor`,
  
  preAggregations: {
    // Pre-Aggregations definitions go here
    // Learn more here: https://cube.dev/docs/caching/pre-aggregations/getting-started  
  },
  
  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [taxIdName, taxIdNo, uid, idNo, name, createdBy, id, createdDate, dateTrx, lastModifiedDate]
    }
  },
  
  dimensions: {
    phone: {
      sql: `phone`,
      type: `string`
    },
    
    processed: {
      sql: `processed`,
      type: `string`
    },
    
    lastModifiedBy: {
      sql: `last_modified_by`,
      type: `string`
    },
    
    taxIdName: {
      sql: `tax_id_name`,
      type: `string`
    },
    
    taxIdNo: {
      sql: `tax_id_no`,
      type: `string`
    },
    
    tin: {
      sql: `tin`,
      type: `string`
    },
    
    documentStatus: {
      sql: `document_status`,
      type: `string`
    },
    
    branch: {
      sql: `branch`,
      type: `string`
    },
    
    location: {
      sql: `location`,
      type: `string`
    },
    
    uid: {
      sql: `uid`,
      type: `string`
    },
    
    code: {
      title: `Vendor Code`,
      sql: `code`,
      type: `string`
    },
    
    documentNo: {
      sql: `document_no`,
      type: `string`
    },
    
    type: {
      sql: `type`,
      type: `string`
    },
    
    fax: {
      sql: `fax`,
      type: `string`
    },
    
    idNo: {
      sql: `id_no`,
      type: `string`
    },
    
    active: {
      sql: `active`,
      type: `string`
    },
    
    name: {
      title: `Vendor Name`,
      sql: `name`,
      type: `string`
    },
    
    createdBy: {
      sql: `created_by`,
      type: `string`
    },
    
    documentAction: {
      sql: `document_action`,
      type: `string`
    },
    
    email: {
      sql: `email`,
      type: `string`
    },
    
    approved: {
      sql: `approved`,
      type: `string`
    },
    
    paymentCategory: {
      sql: `payment_category`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `number`,
      primaryKey: true
    },
    
    website: {
      sql: `website`,
      type: `string`
    },
    
    createdDate: {
      sql: `created_date`,
      type: `time`
    },
    
    dateTrx: {
      sql: `date_trx`,
      type: `time`
    },
    
    lastModifiedDate: {
      sql: `last_modified_date`,
      type: `time`
    }
  },
  
  dataSource: `default`
});
